var flag; //true is right while left is false
var score=0;
var createTime=3000;
var bigfire;
var death = false;
var pause = false;
var fx;
var volume=0.6, width=48;
var first_name="", second_name="", third_name="";
var first=0, second=0, third=0;
var big = false;
var create_time = 30000, remain=0;
var cDown;
var measureTime;

var instructionState = {
    preload: function(){
        game.load.image('background', 'assets/background5.jpg');
        document.getElementById('pause').style.visibility = 'hidden';
        document.getElementById('instruction').style.visibility = 'hidden';
        document.getElementById('heart1').style.visibility = 'hidden';
        document.getElementById('heart2').style.visibility = 'hidden';
        document.getElementById('heart3').style.visibility = 'hidden';
        document.getElementById('b1').style.visibility = 'hidden';
        document.getElementById('b2').style.visibility = 'hidden';
        document.getElementById('b3').style.visibility = 'hidden';
        document.getElementById('continue').style.visibility = 'hidden';
        document.getElementById('score').style.visibility = 'hidden';
        document.getElementById('LeaderBoard').style.visibility = 'hidden';
        document.getElementById('start').style.visibility = 'hidden';
        document.getElementById('restart').style.visibility = 'hidden';
        document.getElementById('name').style.visibility = 'hidden';
        document.getElementById('myProgress').style.visibility = 'hidden';
        document.getElementById('myBar').style.visibility = 'hidden';
        document.getElementById('minus').style.visibility = 'hidden';
        document.getElementById('plus').style.visibility = 'hidden';
        document.getElementById('volumee').style.visibility = 'hidden';
        document.getElementById('myBar2').style.visibility = 'hidden';
        document.getElementById('myProgress2').style.visibility = 'hidden';
        document.getElementById('turntomenu').style.visibility = 'visible';
    },
    create: function(){
        this.background = game.add.tileSprite(0, 0, 650, 700,  'background');
        this.background.scale.setTo(0.78, 0.78); // Se the anchor point of player to center.
    }
};

var leaderState = {
    preload: function(){
        game.load.image('background', 'assets/background4.jpg');
        document.getElementById('pause').style.visibility = 'hidden';
        document.getElementById('heart1').style.visibility = 'hidden';
        document.getElementById('heart2').style.visibility = 'hidden';
        document.getElementById('instruction').style.visibility = 'hidden';
        document.getElementById('heart3').style.visibility = 'hidden';
        document.getElementById('b1').style.visibility = 'hidden';
        document.getElementById('b2').style.visibility = 'hidden';
        document.getElementById('b3').style.visibility = 'hidden';
        document.getElementById('continue').style.visibility = 'hidden';
        document.getElementById('score').style.visibility = 'hidden';
        document.getElementById('LeaderBoard').style.visibility = 'hidden';
        document.getElementById('start').style.visibility = 'hidden';
        document.getElementById('restart').style.visibility = 'hidden';
        document.getElementById('name').style.visibility = 'hidden';
        document.getElementById('myProgress').style.visibility = 'hidden';
        document.getElementById('myBar').style.visibility = 'hidden';
        document.getElementById('minus').style.visibility = 'hidden';
        document.getElementById('plus').style.visibility = 'hidden';
        document.getElementById('volumee').style.visibility = 'hidden';
        document.getElementById('myBar2').style.visibility = 'hidden';
        document.getElementById('myProgress2').style.visibility = 'hidden';
        document.getElementById('turntomenu').style.visibility = 'visible';
    },
    create: function(){
        this.background = game.add.tileSprite(0, 0, 650, 700,  'background');
        this.background.scale.setTo(0.8, 0.8); // Se the anchor point of player to center.
        var postsRef = firebase.database().ref('score');
        postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            snapshot.forEach(function(childSnapshot) {
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();
                if(childData.score > first){
                    third = second;
                    second = first;
                    first = childData.score;
                    third_name = second_name;
                    second_name = first_name;
                    first_name = childData.name;
                }
                else if(childData.score > second){
                    third = second;
                    second = childData.score;
                    third_name = second_name;
                    second_name = childData.name;
                }
                else if(childData.score > third){
                    third = childData.score;
                    third_name = childData.name;
                }
                
                // ...
            });

        })
        .then(function(){
            console.log(first_name + ": " + first);
            console.log(second_name + ": " + second);
            console.log(third_name + ": " + third);
            document.getElementById('one').innerText = "First :   " + first_name + "     " + first;
            document.getElementById('two').innerText = "Second :   " + second_name + "     " + second; 
            document.getElementById('three').innerText = "Third :   " + third_name + "     " + third; 
        })
        .catch(e => console.log(e.message));

        
    
    },
    upload: function(){
        
    }
};

var gameoverState = {
    preload: function() {
        game.load.image('backgroundd', 'assets/gameover.jpg');
        document.getElementById('score').style.visibility = 'hidden';
        document.getElementById('restart').style.visibility = 'visible';
        document.getElementById('pause').style.visibility = 'hidden';
        document.getElementById('continue').style.visibility = 'hidden';
        document.getElementById('minus').style.visibility = 'hidden';
        document.getElementById('plus').style.visibility = 'hidden';
        document.getElementById('b1').style.visibility = 'hidden';
        document.getElementById('instruction').style.visibility = 'hidden';
        document.getElementById('b2').style.visibility = 'hidden';
        document.getElementById('b3').style.visibility = 'hidden';
        document.getElementById('volumee').style.visibility = 'hidden';
        document.getElementById('myBar2').style.visibility = 'hidden';
        document.getElementById('turntomenu').style.visibility = 'hidden';
        document.getElementById('LeaderBoard').style.visibility = 'hidden';
        document.getElementById('myProgress2').style.visibility = 'hidden';
        document.getElementById('name').style.visibility = 'visible';
        document.getElementById('yourscore').innerHTML = "Your Score is: " + score +". Let us know your name!";
        //this.bgm.stop();
    },
    create: function() {
        this.background = game.add.tileSprite(0, 0, 650, 700,  'backgroundd');
        this.background.scale.setTo(0.8, 0.8); // Se the anchor point of player to center.
    },
    upload: function() {
        
    }
};

var menuState = {
    preload: function (){
        game.load.image('background', 'assets/background2.jpg');
        document.getElementById('myBar').style.width = 1;
        document.getElementById('pause').style.visibility = 'hidden';
        document.getElementById('heart1').style.visibility = 'hidden';
        document.getElementById('instruction').style.visibility = 'visible';
        document.getElementById('heart2').style.visibility = 'hidden';
        document.getElementById('b1').style.visibility = 'hidden';
        document.getElementById('b2').style.visibility = 'hidden';
        document.getElementById('b3').style.visibility = 'hidden';
        document.getElementById('heart3').style.visibility = 'hidden';
        document.getElementById('continue').style.visibility = 'hidden';
        document.getElementById('score').style.visibility = 'hidden';
        document.getElementById('LeaderBoard').style.visibility = 'visible';
        document.getElementById('myProgress').style.visibility = 'hidden';
        document.getElementById('turntomenu').style.visibility = 'hidden';
        document.getElementById('myBar').style.visibility = 'hidden';
        document.getElementById('minus').style.visibility = 'visible';
        document.getElementById('plus').style.visibility = 'visible';
        document.getElementById('volumee').style.visibility = 'visible';
        document.getElementById('myBar2').style.visibility = 'visible';
        document.getElementById('myProgress2').style.visibility = 'visible';
        document.getElementById('name').style.visibility = 'hidden';
        score = 0;
    },

    create: function() {
        this.background = game.add.tileSprite(0, 0, 650, 700,  'background');
        this.background.scale.setTo(0.8, 0.8); // Se the anchor point of player to center.
        
    },

    upload: function() {
        
    }

};

var mainState = {

    /// ToDo: Load some images in preload function.
	///		  1. Three images in asset directory
	///			 player.png, bg.png, tree2.png
    preload: function() {
        document.getElementById('pause').style.visibility = 'visible';
        game.load.spritesheet('player', 'assets/airplane2.png', 28, 21);
        game.load.image('background', 'assets/background3.jpg');
        game.load.image('mybullet', 'assets/bullet.png');
        game.load.image('myspecialbullet', 'assets/sbullet.png');
        game.load.image('enemy', 'assets/enemy.png');
        game.load.image('measure', 'assets/measure.png');
        game.load.image('bigenemy', 'assets/ship.png');
        document.getElementById('LeaderBoard').style.visibility = 'hidden';
        document.getElementById('turntomenu').style.visibility = 'hidden';
        game.load.image('enemyBullet', 'assets/enemybullet.png');
        document.getElementById('instruction').style.visibility = 'hidden';
        game.load.audio('bgmmusic', 'assets/bgm.ogg');
        game.load.audio('sfx', 'assets/fx_mixdown.ogg');
        game.load.image('pixel', 'assets/explore2.png');
        game.load.image('pixel2', 'assets/smoke.png');
        game.load.image('pixel3', 'assets/explore3.png');
        document.getElementById('continue').style.visibility = 'hidden';
        document.getElementById('name').style.visibility = 'hidden';
        flag = 0;
    },

    create: function() {

        // ToDo: 1. Add Background image at (0,0) and set scale to (0.8,0.8)
		//       2. Add tree sprite at (50,160) and set scale to (1.5,1.5)
        //       3. Create a cursor key event listener.
        
        game.physics.startSystem(Phaser.Physics.ARCADE);

        
        this.bgm = game.add.audio('bgmmusic');
        this.bgm.loop = true;
        this.bgm.volume = 0.6;
        this.bgm.play();
        fx = game.add.audio('sfx');
        fx.allowMultiple = true;
        fx.addMarker('alien death', 1, 1.0);
        fx.addMarker('boss hit', 3, 0.5);
        fx.addMarker('death', 12, 4.2);
        //this.player.body.gravity.y = 500;

        

        this.background = game.add.tileSprite(0, 0, 650, 700,  'background');
        this.background.scale.setTo(0.8, 0.8); // Se the anchor point of player to center.
        
        //this.tree = game.add.sprite(50, 160, 'tree');
        //this.tree.scale.setTo(1.5, 1.5); // Se the anchor point of player to center.

        this.player = game.add.sprite(game.width/2, 520, 'player');
        this.player.anchor.setTo(0.5, 0.5); // Se the anchor point of player to center.
        this.player.level = 3;
        this.player.animations.add('rightwalk', [1, 2], 10, true);
        /// 2. Create the 'leftwalk' animation with frame rate = 8 by looping the frames 3 and 4
        this.player.animations.add('leftwalk', [3, 4], 10, true);
        this.player.animations.add('noneani', [0, 0], 10, true);
        game.physics.arcade.enable(this.player);

        this.cursor = game.input.keyboard.createCursorKeys();

        this.mybullets = game.add.group();
        this.mybullets.enableBody = true;
        this.mybullets.createMultiple(200, 'mybullet');
        this.mybullets.setAll('outOfBoundsKill', true);
        this.mybullets.setAll('checkWorldBounds', true);
        this.myStartFire = true;
        this.bulletTime = 0;
        game.physics.arcade.enable(this.mybullets);

        this.myspecialbullets = game.add.group();
        this.myspecialbullets.enableBody = true;
        this.myspecialbullets.createMultiple(200, 'myspecialbullet');
        this.myspecialbullets.setAll('outOfBoundsKill', true);
        this.myspecialbullets.setAll('checkWorldBounds', true);
        this.specialbulletTime = 0;
        game.physics.arcade.enable(this.myspecialbullets);

        this.enemys = game.add.group();
        this.enemys.enableBody = true;
        this.enemys.createMultiple(50, 'enemy');
        this.enemys.setAll('outOfBoundsKill', true);
        this.enemys.setAll('checkWorldBounds', true);
        //this.myStartFire = true;
        this.enemyTime = 0;
        game.physics.arcade.enable(this.enemys);

        this.bigenemys = game.add.group();
        this.bigenemys.enableBody = true;
        this.bigenemys.createMultiple(100, 'bigenemy');
        this.bigenemys.setAll('outOfBoundsKill', true);
        this.bigenemys.setAll('checkWorldBounds', true);
        //this.myStartFire = true;
        this.bigenemyTime = 30000;
        game.physics.arcade.enable(this.bigenemys);

        this.measure = game.add.group();
        this.measure.enableBody = true;
        this.measure.createMultiple(100, 'measure');
        this.measure.setAll('outOfBoundsKill', true);
        this.measure.setAll('checkWorldBounds', true);
        //this.myStartFire = true;
        measureTime = 10000;
        game.physics.arcade.enable(this.measure);

        this.emitter = game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;

        this.emitter2 = game.add.emitter(422, 320, 15);
        this.emitter2.makeParticles('pixel2');
        this.emitter2.setYSpeed(-150, 150);
        this.emitter2.setXSpeed(-150, 150);
        this.emitter2.setScale(2, 0, 2, 0, 800);
        this.emitter2.gravity = 500;

        this.emitter3 = game.add.emitter(422, 320, 15);
        this.emitter3.makeParticles('pixel3');
        this.emitter3.setYSpeed(-150, 150);
        this.emitter3.setXSpeed(-150, 150);
        this.emitter3.setScale(2, 0, 2, 0, 800);
        this.emitter3.gravity = 500;
        
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.createMultiple(50, 'enemyBullet');
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);
        game.physics.arcade.enable(this.enemyBullets);

        this.bigenemyBullets = game.add.group();
        this.bigenemyBullets.enableBody = true;
        this.bigenemyBullets.createMultiple(300, 'enemyBullet');
        this.bigenemyBullets.setAll('outOfBoundsKill', true);
        this.bigenemyBullets.setAll('checkWorldBounds', true);
        game.physics.arcade.enable(this.bigenemyBullets);

        bigfire = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        cDown = this.input.keyboard.addKey(Phaser.KeyCode.C);
    },

    /// Call moving function in each frame.
    update: function() {
        this.movePlayer();
        this.fireBullet();
        this.createEnemy();
        this.enemyFire();
        this.createbigEnemy();
        this.createMeasure();
        this.bigenemyFire();
        this.fireSpecialBullet();
        this.background.tilePosition.y += 2;
        game.physics.arcade.overlap(this.mybullets, this.enemys, this.hitEnemy, null, this);
        game.physics.arcade.overlap(this.myspecialbullets, this.enemys, this.killEnemy, null, this);
        game.physics.arcade.overlap(this.enemyBullets, this.player, this.hitMe, null, this);
        game.physics.arcade.overlap(this.mybullets, this.bigenemys, this.hitbigEnemy, null, this);
        game.physics.arcade.overlap(this.myspecialbullets, this.bigenemys, this.killbigEnemy, null, this);
        game.physics.arcade.overlap(this.bigenemyBullets, this.player, this.hitMe, null, this);
        game.physics.arcade.overlap(this.bigenemys, this.player, this.crash, null, this);
        game.physics.arcade.overlap(this.enemys, this.player, this.crash, null, this);
        game.physics.arcade.overlap(this.measure, this.player, this.eatMeasure, null, this);
        if(flag==0){
            flag=1;
            move();
        }
        else if(flag==2 && bigfire.isDown){
            flag=0;
            big = true;
            //document.getElementById('heart1').style.visibility = 'visible';
            //document.getElementById('heart2').style.visibility = 'visible';
            //document.getElementById('heart3').style.visibility = 'visible';
            //this.player.level = 3;
            var idd = setInterval(doo, 4000);
            function doo(){
                big = false;
                clearInterval(idd);
            }
        }
        this.bgm.volume = volume;
        fx.volume = volume;
    },

    hitEnemy: function(myBullet, enemy) {
        this.emitter.x = enemy.x+15;
        this.emitter.y = enemy.y+15;
        this.emitter.start(true, 800, null, 15);
        myBullet.kill();
        fx.play('alien death');
        if(enemy.life > 1) enemy.life = enemy.life - 1;
        else {
            enemy.kill();
            score = score + 10;
            document.getElementById('score').innerText = "Score :  " + score;
            if(createTime > 1000) createTime = createTime - 100;
        }
    },

    killEnemy: function(myBullet, enemy) {
        this.emitter.x = enemy.x+15;
        this.emitter.y = enemy.y+15;
        this.emitter.start(true, 800, null, 15);
        myBullet.kill();
        fx.play('alien death');
        enemy.kill();
        score = score + 10;
        document.getElementById('score').innerText = "Score :  " + score;
        if(createTime > 1000) createTime = createTime - 100;
    },

    killbigEnemy: function(myBullet, enemy) {
        this.emitter.x = enemy.x+15;
        this.emitter.y = enemy.y+15;
        this.emitter.start(true, 800, null, 15);
        myBullet.kill();
        fx.play('alien death');
        enemy.kill();
        score = score + 100;
        document.getElementById('score').innerText = "Score :  " + score;
        if(createTime > 1000) createTime = createTime - 100;
    },

    hitbigEnemy: function(myBullet, enemy) {
        this.emitter.x = enemy.x+15;
        this.emitter.y = enemy.y+15;
        this.emitter.start(true, 800, null, 15);
        myBullet.kill();
        fx.play('alien death');
        if(enemy.life > 1) enemy.life = enemy.life - 1;
        else {
            enemy.kill();
            score = score + 100;
            document.getElementById('score').innerText = "Score :  " + score;
            if(createTime > 1000) createTime = createTime - 100;
        }
    },

    eatMeasure: function(player, m){
        m.kill();
        remain = 3;
        
        document.getElementById('b1').style.visibility = 'visible';
        document.getElementById('b2').style.visibility = 'visible';
        document.getElementById('b3').style.visibility = 'visible';
    },

    hitMe: function(player, enemyBullet) {
        this.emitter2.x = player.x;
        this.emitter2.y = player.y;
        this.emitter3.x = player.x;
        this.emitter3.y = player.y;
        enemyBullet.kill();
        if(player.level == 3){
            fx.play('boss hit');
            player.level = player.level - 1;
            document.getElementById('heart1').style.visibility = 'hidden';
            this.emitter2.start(true, 800, null, 15);
        }
        else if(player.level == 2){
            fx.play('boss hit');
            player.level = player.level - 1;
            document.getElementById('heart2').style.visibility = 'hidden';
            this.emitter2.start(true, 800, null, 15);
        }
        else if(player.level == 1){
            this.emitter3.start(true, 800, null, 15);
            fx.play('death');
            player.level = player.level - 1;
            document.getElementById('heart3').style.visibility = 'hidden';
            this.bgm.stop();
            toStart('3');
        }
    },

    crash: function(player, enemy) {
        this.emitter3.x = player.x;
        this.emitter3.y = player.y;
        if(player.level == 3){
            //fx.play('boss hit');
            player.level = player.level - 1;
            document.getElementById('heart1').style.visibility = 'hidden';
            this.emitter2.start(true, 800, null, 15);
        }
        if(player.level == 2){
            //fx.play('boss hit');
            player.level = player.level - 1;
            document.getElementById('heart2').style.visibility = 'hidden';
            this.emitter2.start(true, 800, null, 15);
        }
        if(player.level == 1){
            this.emitter3.start(true, 800, null, 15);
            fx.play('death');
            player.level = player.level - 1;
            document.getElementById('heart3').style.visibility = 'hidden';
            this.bgm.stop();
            player.kill();
            enemy.kill();
            toStart('3');
        }
    },

    enemyFire() {
        this.enemys.forEachExists(function(enemy) {
			var bullet = this.enemyBullets.getFirstExists(false);
            if(bullet) {
                if(game.time.now > (enemy.bulletTime || 0)) {
                    bullet.reset(enemy.x+12 , enemy.y);
                    bullet.body.velocity.y = +300;
                    enemy.bulletTime = game.time.now + createTime*0.4;
                }
            }
		}, this);
    },

    bigenemyFire() {
        this.bigenemys.forEachExists(function(bigenemy) {
			var bullet = this.enemyBullets.getFirstExists(false);
            if(bullet) {
                if(game.time.now > (bigenemy.bulletTime || 0)) {
                    bullet.reset(bigenemy.x+20 , bigenemy.y+100);
                    bullet.body.velocity.y = +300;
                }
            }
            bullet = this.enemyBullets.getFirstExists(false);
            if(bullet) {
                if(game.time.now > (bigenemy.bulletTime || 0)) {
                    bullet.reset(bigenemy.x+120 , bigenemy.y+100);
                    bullet.body.velocity.y = +300;
                    //bigenemy.bulletTime = game.time.now + 100;
                }
            }
            bullet = this.enemyBullets.getFirstExists(false);
            if(bullet) {
                if(game.time.now > (bigenemy.bulletTime || 0)) {
                    bullet.reset(bigenemy.x+220 , bigenemy.y+100);
                    bullet.body.velocity.y = +300;
                    //bigenemy.bulletTime = game.time.now + 100;
                }
            }
            bullet = this.enemyBullets.getFirstExists(false);
            if(bullet) {
                if(game.time.now > (bigenemy.bulletTime || 0)) {
                    bullet.reset(bigenemy.x+320 , bigenemy.y+100);
                    bullet.body.velocity.y = +300;
                    //bigenemy.bulletTime = game.time.now + 100;
                }
            }
            bullet = this.enemyBullets.getFirstExists(false);
            if(bullet) {
                if(game.time.now > (bigenemy.bulletTime || 0)) {
                    bullet.reset(bigenemy.x+420 , bigenemy.y+100);
                    bullet.body.velocity.y = +300;
                    bigenemy.bulletTime = game.time.now + 100;
                }
            }
		}, this);
    },

    createEnemy() {
        if(game.time.now > this.enemyTime) {
            var e;
            e = this.enemys.getFirstExists(false);
            if(e) {
              e.reset(this.player.x-30 , 0);
              e.body.velocity.y = +100;
              e.life = 2;
              this.enemyTime = game.time.now + createTime*1.1;
            }
        }
    },

    createbigEnemy() {
        if(game.time.now > create_time) {
            var e;
            e = this.bigenemys.getFirstExists(false);
            if(e) {
              e.reset(100 , -120);
              e.body.velocity.y = +20;
              e.life = 300;
              create_time = game.time.now + 30000;
            }
        }
    },

    createMeasure() {
        if(game.time.now > measureTime) {
            var e;
            e = this.measure.getFirstExists(false);
            if(e) {
                var x = Math.floor((Math.random()*350));
                //console.log(x);
                e.reset(x+50 , 0);
                e.body.velocity.y = +50;
                measureTime = game.time.now + 29000;
            }
        }
    },

    fireBullet: function() {
        if(game.time.now > this.bulletTime) {
            var bullet;
            bullet = this.mybullets.getFirstExists(false);
            if(bullet) {
                var toFire = (big==true)? 20 : 200;
                bullet.reset(this.player.x -3 , this.player.y - 15);
                bullet.body.velocity.y = (big==true)? -1000 : -400;
                this.bulletTime = game.time.now + toFire;
            }
            if(big){
                bullet = this.mybullets.getFirstExists(false);
                if(bullet) {
                    bullet.reset(this.player.x -3 , this.player.y - 15);
                    bullet.body.velocity.y = -1000;
                    bullet.body.velocity.x = -200;
                }
                bullet = this.mybullets.getFirstExists(false);
                if(bullet) {
                    bullet.reset(this.player.x -3 , this.player.y - 15);
                    bullet.body.velocity.y = -1000;
                    bullet.body.velocity.x = +200;
                }
            }
        }
    },

    fireSpecialBullet: function() {
        if(game.time.now > this.specialbulletTime && remain >0 && cDown.isDown) {
            remain = remain - 1;
            var bullet;
            bullet = this.myspecialbullets.getFirstExists(false);
            if(bullet) {
                var toFire = (big==true)? 20 : 710;
                bullet.reset(this.player.x -10 , this.player.y - 15);
                bullet.body.velocity.y = (big==true)? -1000 : -400;
                this.specialbulletTime = game.time.now + toFire;
            }
            if(remain == 2){
                document.getElementById('b1').style.visibility = 'hidden';
                document.getElementById('b2').style.visibility = 'visible';
                document.getElementById('b3').style.visibility = 'visible';
            }
            else if(remain == 1){
                document.getElementById('b1').style.visibility = 'hidden';
                document.getElementById('b2').style.visibility = 'hidden';
                document.getElementById('b3').style.visibility = 'visible';
            }
            else if(remain == 0){
                document.getElementById('b1').style.visibility = 'hidden';
                document.getElementById('b2').style.visibility = 'hidden';
                document.getElementById('b3').style.visibility = 'hidden';
            }
        }
    },

    /// ToDo: Complete the function of moving a player.
	///       Remember to flip player's sprite when it change horizontal direction
    movePlayer: function() {
        if (this.cursor.left.isDown && this.player.x > 27){
            this.player.x -= 4;
            this.player.animations.play('leftwalk'); 
            /*
            if(flag) {
                this.player.scale.x *= -1;
                flag = !flag;
            }
            */
        } 
        else if(this.cursor.right.isDown && this.player.x < 473){
            this.player.x += 4;
            this.player.animations.play('rightwalk');
            /*
            if(!flag) {
                this.player.scale.x *= -1;
                flag = !flag;
            }
            */
        }
        else if(this.cursor.up.isDown) {
            this.player.y -= 4;
            this.player.animations.play('noneani'); 
        }
        else if(this.cursor.down.isDown && this.player.y < 524) {
            this.player.animations.play('noneani'); 
            this.player.y += 4;
        }
        else  this.player.animations.play('noneani'); 
    }
};

function move() {
    var elem = document.getElementById("myBar");   
    var width = 1;
    var id = setInterval(frame, 100);
    function frame() {
        if(death){
            clearInterval(id);
            death = false;
            elem.style.width = 1 + "px";
        }
        else if(pause);
        else if (width >= 150) {
            clearInterval(id);
            flag=2;
        } else {
            width++; 
            elem.style.width = width + "px"; 
        }
    }
};

async function toStart(str) {
    if(str=='1'){
        document.getElementById('score').innerHTML = "Score :  0";
        document.getElementById('heart1').style.visibility = 'visible';
        document.getElementById('heart2').style.visibility = 'visible';
        document.getElementById('heart3').style.visibility = 'visible';
        document.getElementById('score').style.visibility = 'visible';
        document.getElementById('myProgress').style.visibility = 'visible';
        document.getElementById('myBar').style.visibility = 'visible';
        document.getElementById('start').style.visibility = 'hidden';
        create_time = game.time.now + 30000;
        measureTime = game.time.now + 10000;
        createTime = 3000;
        game.state.start('main');
    }
    else if(str=='3'){
        document.getElementById('myBar').style.width = 1;
        document.getElementById('score').style.visibility = 'hidden';
        document.getElementById('myProgress').style.visibility = 'hidden';
        document.getElementById('myBar').style.visibility = 'hidden';
        game.state.start('gameover');
        death = true;
    }
    else if(str=='2'){ // restart
        
        var post_txt = document.getElementById('name');
        if (post_txt.value != "") {
            var postListRef = firebase.database().ref('score');
            var newPostRef = postListRef.push();

            await newPostRef.set({
                "name": post_txt.value,
                "score": score
            });
            console.log(post_txt.value);
        }
        document.getElementById('restart').style.visibility = 'hidden';
        document.getElementById('start').style.visibility = 'visible';
        document.getElementById('yourscore').innerHTML = "";
        game.state.start('menu');
        document.getElementById('one').innerHTML = "";
        document.getElementById('two').innerHTML = "";
        document.getElementById('three').innerHTML = "";
        first = 0;
        second = 0;
        third = 0;
        flag = 0;
        death = false;
    }
    else if(str=='4'){
        game.state.start('leader');
    }
    else if(str=='5'){
        game.state.start('instruction');
    }
};

function toPause(){
    pause = !pause;
    if(pause == true) {
        document.getElementById('pause').style.visibility = 'hidden';
        document.getElementById('continue').style.visibility = 'visible';
        game.paused = true;
    }
    else {
        game.paused = false;
        document.getElementById('pause').style.visibility = 'visible';
        document.getElementById('continue').style.visibility = 'hidden';
    }
}

function judge_volume(str){
    if(str=='+' && volume<=0.9){
        volume = volume + 0.1;
        width = width + 8;
        document.getElementById('myBar2').style.width = width + "px";
    }
    else if(str=='-' && volume >= 0.1){
        width = width - 8;
        document.getElementById('myBar2').style.width = width + "px";
        volume = volume - 0.1;
    }
}

/// ToDo: 1. Initialize Phaser (width: 500, height: 340) 
///		  2. Add our state(mainState)
///		  3. Start it!!

var game = new Phaser.Game(500, 550, Phaser.AUTO, 'canvas');
game.state.add('main', mainState);
game.state.add('gameover', gameoverState);
game.state.add('instruction', instructionState);
game.state.add('menu', menuState);
game.state.add('leader', leaderState);
game.state.start('menu');
//game.state.start('leader');


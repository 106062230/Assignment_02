# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 

    * level: 遊戲的難易度會隨著score越來越高，而變得越來越難，敵人出現的時間間隔會變短，敵人射擊子彈的速度也會變快，之後也會出現boss。

    * Skill: 右上角有集氣條，當集氣條滿了之後可點選鍵盤上的SPACE鍵使用大絕，大絕持續的時間大約4秒。


2. Animations : 

    * 當player向左或向右移動的時候會有小動畫，其他時候都是朝向上方的。

3. Particle Systems : 

    * my bullet: 當player的子彈射擊到敵人時，會產生火花。

    * enemy's bullet: 當敵人的子彈射擊到player時，若player尚未死亡，會產生煙霧，若是剛好使player死亡，便會產生爆炸的火花。

4. Sound effects : 

    * 當遊戲進行時會有bgm播放。

    * 射擊到enemy會有音效。

    * 射擊到player會有另一種音效。

    * 可在遊戲介面的左上角調整音量大小，點選+的圖片時音量會增加，點選-的圖片時音量會降低，底下的長方形是表示目前音量的大小。

5. Leaderboard : 

    leaderboard可顯示此遊戲中分數最高的前3名，但若有玩家分數很高，但最終沒有留下名字的話，便不會記錄此分數。


# Bonus Functions Description : 
1. Unique bullet : 當player吃到寶箱之後，會將火球子彈填滿至3發（有幾發可以看右上角的火球樹木數目），點選鍵盤上的C鍵後不論敵人的血條有多少，皆可直接殺死敵人。
2. Boss : 每隔30秒會出現boss，boss的體型非常寬，並且會一次射擊4發子彈，射擊速度也很快，血量也非常多，若不使用大絕或火焰子彈，死亡機率就非常高。
